Feature: Performing a login

        

    Scenario Outline: 
        Given I am on the login page
        When I login with username and password <user> <password> into the text box
        Then I should see a welcome message

        Examples:
        |user| |password|
        |"wl.interview.session@gmail.com"| |"WL2021&*"|
