const { Given, When, Then } = require('@wdio/cucumber-framework');

const LoginPage = require('../pageobjects/login.page');
const SecurePage = require('../pageobjects/secure.page');

const pages = {
    login: LoginPage
}

Given(/^I am on the (\w+) page$/, async (page) => {
    await pages[page].open()
});

When(/^I login with username and password "([^"]*)" "([^"]*)" into the text box$/, async (arg1, arg2) => {
    await LoginPage.login(arg1, arg2);    // entering user name, password and and submiting the page
});

Then(/^I should see a welcome message/, async () => {
    await expect(SecurePage.flashAlert).toBeExisting();
});

